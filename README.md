# Training YOLOv7 on the KSU HPC

## Foreword:

The process described below is what us at the [Kennesaw State Electric Vehicle Team](https://owllife.kennesaw.edu/organization/evt) have used to train [YOLOv7](https://github.com/WongKinYiu/yolov7) to recognize orange traffic cones. YOLOv7 is a state-of-the-art object detection model which can be trained to recognize just about anything (if provided with enough training data).


<div align="center">
    <a href="./">
        <img src="https://raw.githubusercontent.com/WongKinYiu/yolov7/main/figure/performance.png" width="79%"/>
    </a>
</div>

While it's unlikely you the reader share our passionate obsession on traffic cones, you may easily adapt the process to other objects to suit your own research needs

## Process

Ethan and I made a dataset composed of a ratio of [3 FSOCO images for every 1 of our custom-labeled images](https://gitlab.com/KSU_EVT/autonomous-software/merge_and_segment_training_data) of traffic cones [+](https://gitlab.com/KSU_EVT/autonomous-software/obtain_training_images) [+](https://gitlab.com/KSU_EVT/autonomous-software/supervisely_json_to_yolo_txt). The images are split evenly into 5 folders named `0`, `1`, `2`, `3`, and `4`, containing just over 12,000 images total. Each of these directories should contain an `images` folder and a `labels` folder. In the YOLO training config yaml file, only the `images` path must be given for each segment, yolo will automatically replace `labels` for `images` to find the labels for use in training.

Preliminary results training on this dataset with YOLOv7-tiny have been very good. Earlier failures with YOLOv7-tiny were likely due to limited training data or poor training methodology.

The high level overview is that we would like to train on 4/5th's of the data for any one of four runs, while the un-used fifth will be used as a validation set. For each subsequent run, the neural net weight ouput from the previous run will be used as input for the next run and the segment of data used as the validation set is rotated. We will forgo a fifth and final run for our dataset as most of our custom images in the `4` directory do not have labels, and therefore would perform poorly as a validation set.

First, you must transfer the cone training data from your local machine to the remote HPC. `rsync` is the best utility for this:
```bash
rsync -rtvP [source]/ksuevt-cone-training-data-alpha-0.tar.gz [destination]
```

Then, unpack the tarball:
```bash
tar –xvzf ksuevt-cone-training-data-alpha-0.tar.gz
```

The tarball I distributed had a flaw, the images and labels in `mixed/0`, `mixed/1`, `mixed/2`, `mixed/3` and `mixed/4` were not placed in their own `images` and `labels` sub folder. You can fix this by running the following line while cd'd into each of the folders `0`, `1`, `2`, `3`, and `4`
```bash
mkdir labels; mkdir images; mv *.png images; mv *.jpg images; mv *.txt labels
```

Next, you must transfer the tarball containing some customized yolo config `yaml` files and an updated hyperparameters `yaml` file. Again, `rsync` is the best utility for this:
```bash
rsync -rtvP [source]/cone-training-config-and-hyp.tar.xz [destination]
```

Then, unpack the tarball:
```bash
tar -xf cone-training-config-and-hyp.tar.xz
```

Clone [WongKinYui/yolov7](https://github.com/WongKinYiu/yolov7) from GitHub into your home directory on the HPC

I have written four training config files, `custom0.yaml`, `custom1.yaml`, `custom2.yaml`, and `custom3.yaml`. These must be copied to the `yolov7/data` directory:
```bash
cp ~/*.yaml ~/yolov7/data
```

The first run must be performed with `custom0.yaml`. This file must be modified so the path(s) to training data are appropriate for the system. After making such modifications yourself, cd into the yolov7 directory:
```bash
cd ~/yolov7
```

We need to set a python virtualenv and install the prerequisites for yolov7. Run the following:
```bash
python3.6 -m venv yolov7
source yolov7/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Run the following command to download the default `yolov7-tiny.pt` neural net weights file:
```bash
wget -O ~/yolov7/yolov7-tiny.pt https://github.com/WongKinYiu/yolov7/releases/download/v0.1/yolov7-tiny.pt
```

Next, run the following command to download a patch file to fix some things in the yolov7 code for running on the HPC:
```bash
wget -O ~/yolov7/hpc-fix.patch https://gitlab.com/KSU_EVT/autonomous-software/hpc_training_setup/-/raw/master/hpc-fix.patch
```

Now apply the patch to your clone of yolov7:
```bash
cd ~/yolov7; git apply hpc-fix.patch
```

_if there is no output, the patch has been applied succesfully_

Next, create a [TORQUE](https://hpcdocs.kennesaw.edu/torque/) script file to define your job for running on the HPC. For example:


`cones_job_0.script`
```bash
#!/bin/bash
#PBS -k o
#PBS -l nodes=1:ppn=4:gpus=2,walltime=30:00:00
#PBS -M YOUR_EMAIL@students.kennesaw.edu
#PBS -m abe
#PBS -N cones_job_0
#PBS -j oe
#PBS -q gpuq
module load CUDA
module load git
cd yolov7
source /gpfs/SHPC_Data/home/mkrupcza/yolov7/yolov7/bin/activate
python3 /gpfs/SHPC_Data/home/mkrupcza/yolov7/train.py --workers 4 --device 0,1 --batch-size 64 --data /gpfs/SHPC_Data/home/mkrupcza/yolov7/data/custom0.yaml --img 416 416 --cfg /gpfs/SHPC_Data/home/mkrupcza/yolov7/cfg/training/yolov7-tiny.yaml --weights 'yolov7-tiny.pt' --name cones_job_0 --hyp /gpfs/SHPC_Data/home/mkrupcza/yolov7/data/hyp.scratch.tiny.cone-training.yaml --epochs 150 --multi-scale
```

Save the script above into your yolov7 directory. Add your job to the scheduler with the following command:
```bash
qsub cones_job_0.script
```

You will receive an email when the job starts, and another when the job exits with error or it completes. By default, the job will output its combined standard error and standard output to a file in your home directory `~`. You may follow the progress of the job by following this file (replace the X's as appropriate):
```bash
tail -f ~/cones_job_0.oXXXXX
```

This run may take anywhere from a few hours to a few days depending on the system's capabilities.

If you encounter an out of memory error near the end of the 0th epoch, halve your batch size. Repeat this as necessary.

Once it is completed, the program will print out the location to a `best.pt` pytorch weights file representing the neural net with the best performance. This will be used for the next training run `--weights` flag instead of `'yolov7-tiny.pt` (notice we also replace `custom0.yaml` with `custom1.yaml`, and `--name` `yolov7-tiny-custom0` to `yolov7-tiny-custom1`):

(you may have to modify the `--workers` and `--device` flag depending on the number of processors on your system)


`cones_job_1.script`
```bash
#!/bin/bash
#PBS -k o
#PBS -l nodes=1:ppn=4:gpus=2,walltime=30:00:00
#PBS -M YOUR_EMAIL@students.kennesaw.edu
#PBS -m abe
#PBS -N cones_job_1
#PBS -j oe
#PBS -q gpuq
module load CUDA
module load git
cd yolov7
source /gpfs/SHPC_Data/home/mkrupcza/yolov7/yolov7/bin/activate
python3 /gpfs/SHPC_Data/home/mkrupcza/yolov7/train.py --workers 4 --device 0,1 --batch-size 64 --data /gpfs/SHPC_Data/home/mkrupcza/yolov7/data/custom1.yaml --img 416 416 --cfg /gpfs/SHPC_Data/home/mkrupcza/yolov7/cfg/training/yolov7-tiny.yaml --weights 'best0.pt' --name cones_job_1 --hyp /gpfs/SHPC_Data/home/mkrupcza/yolov7/data/hyp.scratch.tiny.cone-training.yaml --epochs 150 --multi-scale
```

Test the `best.pt` model on some new images of your target object (images that were never used for training). If the performance is acceptable, it may be best to just use this model. It is possible that further training will only cause overfitting and degrade performance.

Nevertheless, if you choose to proceed: repeat this process two more times, modifying your next jobs to use `custom2.yaml` and `custom3.yaml` and feed the `best.pt` weights output from the previous run as input for the next one.

Once all is said and done, this generates an accurate, small, and performant cone detection model. The scripts above can be modified for many other purposes depening on your research needs. Your model will only be as good as its dataset, which should be as large as (in)humanly possible. Make sure to have at least around 1500 images per class and at least 10000 instances (of labeled objects) per class. For more information, follow the guidlines linked below (originally for yolov5, which is simmilar to yolov7):


https://github.com/ultralytics/yolov5/wiki/Tips-for-Best-Training-Results


Written for distribution by Matthew Krupczak, with assistance by Sahan Reddy, Ethan Leitner, and Yonnas Alemu
